package com.mindvalley.prj;

/**
 * @author rkviswanath
 * 
 * The Class GameOfLife.
 * 		1 - represents live state
 * 		0 - represents dead state
 * 
 * 		Starting Lives in Gen## : 0
 * 		1000
 * 		0110
 * 		1100
 * 		
 * 		Lives in Gen## : 1
 * 		0100
 * 		0010
 * 		1110
 * 		
 * 		Lives in Gen## : 2
 * 		0000
 * 		1010
 * 		0110
 * 		
 * 		Lives in Gen## : 3
 * 		0000
 * 		0010
 * 		0110
 */
public class GameOfLife {

	/** The initial board. */
	static int[][] initialBoard = { { 1, 0, 0, 0 }, { 0, 1, 1, 0 }, { 1, 1, 0, 0 } };

	/** The no of generations */
	static int noOfGen = 3;

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		GameOfLifeImpl gol = new GameOfLifeImpl();
		Game gme = new Game();
		gme.setRows(initialBoard.length);
		gme.setCols(initialBoard[0].length);

		Cell start[][] = new Cell[gme.getRows()][gme.getCols()];
		for (int i = 0; i < gme.getRows(); i++) {
			for (int j = 0; j < gme.getCols(); j++) {
				start[i][j] = new Cell(i, j, initialBoard[i][j]);
			}
		}
		gme.setCells(start);
		gol.setGame(gme);
		System.out.println("Lives in Gen## : 0");
		gol.showGame();
		for (int gen = 0; gen < noOfGen; gen++) {
			gol.getNextGen();
			System.out.println("Lives in Gen## : " + (gen + 1));
			gol.showGame();
		}
	}
}

/**
 * The Interface iGameOfLife.
 */
interface iGameOfLife {
	public Cell[][] getNextGen();

	public int getNeighbor(Cell cell);

	public int getNextLife(Cell cell);

	public void showGame();
}

/**
 * The Class GameOfLifeImpl.
 */
class GameOfLifeImpl implements iGameOfLife {

	Game gme = null;
	int ngbr = -1;

	public GameOfLifeImpl() {
	}

	/**
	 * Sets the game.
	 *
	 * @param gme
	 *            the new game
	 */
	public void setGame(Game gme) {
		this.gme = gme;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mindvalley.prj.iGameOfLife#getNeighbor(com.mindvalley.prj.Cell)
	 */
	@Override
	public int getNeighbor(Cell cell) {
		int count = 0;
		int minRow = cell.getX() == 0 ? 0 : cell.getX() - 1;
		int maxRow = cell.getX() == (this.gme.getRows() - 1) ? (this.gme.getRows() - 1) : cell.getX() + 1;
		int minCol = cell.getY() == 0 ? 0 : cell.getY() - 1;
		int maxCol = cell.getY() == (this.gme.getCols() - 1) ? (this.gme.getCols() - 1) : cell.getY() + 1;
		for (int row_idx = minRow; row_idx <= maxRow; row_idx++) {
			for (int idx = minCol; idx <= maxCol; idx++) {
				if ((this.gme.getCells()[row_idx][idx].getVal() == 1)
						&& !(row_idx == cell.getX() && idx == cell.getY())) {
					count++;
				}
			}
		}
		/// System.out.println(cell.getX() + " " + cell.getY() + " " + count);
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mindvalley.prj.iGameOfLife#showGame()
	 */
	@Override
	public void showGame() {
		for (Cell cells[] : this.gme.getCells()) {
			for (Cell cell : cells) {
				System.out.print(cell.getVal());
			}
			System.out.println();
		}
		System.out.println();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mindvalley.prj.iGameOfLife#getNextGen()
	 */
	@Override
	public Cell[][] getNextGen() {
		Cell next[][] = new Cell[this.gme.getRows()][this.gme.getCols()];
		for (int i = 0; i < this.gme.getRows(); i++) {
			for (int j = 0; j < this.gme.getCols(); j++) {
				next[i][j] = new Cell(i, j, getNextLife(this.gme.getCells()[i][j]));
			}
		}
		this.gme.setCells(next);
		return next;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mindvalley.prj.iGameOfLife#getNextLife(com.mindvalley.prj.Cell)
	 */
	@Override
	public int getNextLife(Cell cell) {
		return (((ngbr = getNeighbor(cell)) == 3 ? (cell.getVal() == 0 ? 1 : 1)
				: ngbr == 2 ? (cell.getVal() == 1 ? 1 : 0) : 0));
	}
}

/**
 * The Class Cell.
 */
class Cell {
	int x;
	int y;
	int val;

	public Cell(int x, int y) {
		this.x = x;
		this.y = y;
		this.val = 0;
	}

	public Cell(int x, int y, int val) {
		this.x = x;
		this.y = y;
		this.val = val;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setVal(int val) {
		this.val = val;
	}

	public int getVal() {
		return this.val;
	}
}

/**
 * The Class Game.
 */
class Game {
	Cell cells[][];
	int rows;
	int cols;

	public Cell getCell(int x, int y) {
		return this.cells[x][y];
	}

	public Cell[][] getCells() {
		return cells;
	}

	public void setCells(Cell[][] cells) {
		this.cells = cells;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getCols() {
		return cols;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}
}