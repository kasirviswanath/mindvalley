package com.mindvalley.prj;
/**
 * Converts an URL into its short form
 * Uses HashMap to store the URL keys
 * 
 *  Example :-
 *  Original URL					  Short URL
 *  http://www.mindvalley.com/        neqrmeb
 *  
 */
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * @author rkviswanath
 *
 */
class URLProcessor {

	static ConcurrentHashMap<String, URL> hm = new ConcurrentHashMap<String, URL>();
	static Random r = new Random();

	/**
	 * Convert to chars.
	 *
	 * @param str
	 *            the str
	 * @return the int
	 */
	public static int convertToNbr(String str) {
		int num = 0;
		for (int i = 0, len = str.length(); i < len; i++) {
			num += str.charAt(i);
		}
		System.out.println(num);
		return num;
	}

	/**
	 * Convert to Decimal.
	 *
	 * @param nbr
	 *            the nbr
	 * @return the int
	 */
	public static int convertToDecimal(int nbr) {
		return Integer.valueOf(nbr + "", 10);
	}

	/**
	 * Convert to string.
	 *
	 * @param num
	 *            the num
	 * @return the string
	 */
	public static String getShortURL(int num) {
		StringBuilder sb = new StringBuilder();
		while (num > 0) {
			num--;
			int remainder = num % 26;
			char digit = (char) (remainder + 97);
			sb.append(digit);
			num = (num - remainder) / 26;
		}
		return sb.toString();
	}

	/**
	 * Convert to string.
	 *
	 * @return the string
	 */
	public static int getRandomNumber() {
		return r.nextInt(Integer.MAX_VALUE);
	}

	/**
	 * Process url.
	 *
	 * @param input
	 *            the input
	 * @return the string
	 */
	public static String processURL(String input) {
		String output = "";
		int val = getRandomNumber();
		while (hm.contains(val))
			val = getRandomNumber();
		URL url = new URLProcessor().new URL();
		output = getShortURL(convertToDecimal(val));
		url.setOriginalURL(input);
		url.setShortURL(output);
		hm.put(String.valueOf(val), url);
		return output;
	}

	class URL {
		String originalURL;
		String shortURL;

		public String getOriginalURL() {
			return originalURL;
		}

		public void setOriginalURL(String originalURL) {
			this.originalURL = originalURL;
		}

		public String getShortURL() {
			return shortURL;
		}

		public void setShortURL(String shortURL) {
			this.shortURL = shortURL;
		}
	}

}

/**
 * The Class UrlShortener.
 */
public class UrlShortener {
	/**
	 * The main method.
	 *
	 * @param a
	 *            the arguments
	 */
	public static void main(String a[]) {
		String url = "http://www.mindvalley.com/";
		String sUrl = URLProcessor.processURL(url);
		System.out.println(url + "        " + sUrl);
	}

}